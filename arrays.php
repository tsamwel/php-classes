<?php

echo "<br>---- Opgave 6<br>";
$producten = [];
$producten[0] = "boeken";
$producten[1] = "CD's";
$producten[2] = "Smartphones";
$producten[3] = "DVD's";

echo "<br>---- Opgave 7<br>";
print_r($producten); //snel de inhoud van een array printen

echo "<br>---- var_dump(): <br>";
var_dump($producten); //snel de inhoud van een array printen

echo "<br>---- Opgave 8 <br>";
unset($producten[2]); // element verwijderen met index
print_r($producten);

echo "<br>---- Opgave 9 <br>";
$gevonden = array_key_exists(1, $producten); //key check true / false
echo "key 1 gevonden?: ".$gevonden;

echo "<br>---- Opgave 10 <br>";
$gevonden = in_array('boeken', $producten); //chech of de waarde in de array staat. true/false
echo "Boeken gevonden?" .$gevonden;

echo "<br>---- Opgave 11 <br>";
$index = array_search("CD's", $producten); //search for value, return the index.
echo "de index van CD's is" .$index;

echo "<br>---- Opgave 12 <br>";
array_push($producten, "laptops", "tablets"); // add to end of array
print_r($producten);

echo "<br>---- Opgave 13 <br>";
$laatsteElement = array_pop($producten);//laatste element verwijderen
echo $laatsteElement . " is verwijderd:";
print_r($producten);

echo "<br>---- Opgave 14 <br>";
$eersteElement = array_shift($producten);//verwijder eerste array element
echo $eersteElement. " verwijderd:";
print_r($producten);

echo "<br>---- Opgave 15 <br>";
array_unshift($producten, "TV's", "Stereo's");	//voeg element aan begin van array toe
echo "TV's en Stereo's toegevoegd: ";
print_r($producten);

echo "<br>---- Opgave 16 <br>";
$random_keys = array_rand($producten, 2);
echo " eerste random product: ".$producten[$random_keys[0]]; //random element?
echo "<br> tweede random product: ".$producten[$random_keys[1]];

echo "<br>---- Opgave 17 <br>";
function printArray($item, $key){  
	echo "<br> $key" . ": " . "<i> $item </i>";
}
array_walk($producten, 'printArray');  //array doorlopem per element. functie uitvoeren.


echo "<br>---- Opgave 18 ";
$getallen = [
	"nul",
	"een",
	"twee",
	"drie",
	"vier",
	"vijf"
];

$tools = [
	"boek",
	"pen",
	"laptop",
	"potlood"
];

$tekst1 = implode("*", $getallen); //array in string veranderen. met delimeter
echo "<br>--- Opgave 18: Array getallen in tekst1 converteren: $tekst1";
$tekst2 = implode("|", $tools);
echo "<br>--- Opgave 18: Array tools in tekst2 converteren: $tekst2 <br>";


echo "<br>---- Opgave 19 ";
$array1 = explode("*", $tekst1); //reverse van implode
echo "<br>--- Opgave 19: tekst1 in array1 converteren:";
array_walk($array1, 'printArray');

$array2 = explode("|", $tekst2);
echo "<br>--- Opgave 19: tekst2 in array2 converteren:";
array_walk($array2, 'printArray');

echo "<br>---- Opgave  20: boeken-array aangemaakt.<br>";
$boeken = array (
	array(
		"titel" => "Stoner",
		"autheur" => "John Williams",
		"genre" => "fictie",
		"prijs" => 19.99
	),
	array(
		"titel" => "De cirkel",
		"autheur" => "Dave Eggers",
		"genre" => "fictie",
		"prijs" => 22.50
	),
	array(
		"titel" => "Rayuela",
		"autheur" => "Julio Cortazar",
		"genre" => "fictie",
		"prijs" => 25.50
	)
);
echo "<br>---- Opgave  21: boeken recursief doorlopen:<br>";
array_walk_recursive($boeken, 'printArray');  //2D array doorlopen. callback functie

echo "<br>---- Opgave  22: Twee arrays samenvoegen: <br>";
$nieuweboeken = array(
	array(
		"titel" => "spijt",
		"autheur" => "carry slee",
		"genre" => "fictie",
		"prijs" => 12.99
	),
	array(
		"titel" => "debet",
		"autheur" => "saskia noort",
		"genre" => "fictie",
		"prijs" => 33.50
	)
);
$boeken = array_merge($boeken, $nieuweboeken);
array_walk_recursive($boeken, 'printArray');


echo "<br>---- Opgave  23: Array elementen kopieren: <br>";
$oudeboeken = array_slice($boeken, 0, 3);
array_walk_recursive($oudeboeken, 'printArray');


//useful for test?
$string = "13.56";
$int = intval($string); // convert naart int
$double = doubleval($string); // convert to double
$type = gettype($string); // tells you datatype
$setToDouble settype($string, double); // set to double 











