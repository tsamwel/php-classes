<?php

$gewerkteuren = 50;
$uurtarief = 15.00;
$bonus = 100;
$bruto = $gewerkteuren * $uurtarief;

if ($gewerkteuren < 40) {
	echo "Uw basissalaris is: &euro;".$bruto;
	echo "<br>Uw belasting is : &euro;". 0.40*$bruto;
} elseif ($gewerkteuren > 40 && $gewerkteuren < 50) {
	$bruto = $bruto + $bonus;
	echo "Uw Salaris is: &euro;". 0.55*$bruto;
} elseif ($gewerkteuren >= 50) {
	$bruto = $bruto + $bonus;
	$belasting = $bruto * 0.45;
	echo "Uw basissalaris met bonus is: &euro;". $bruto;
	echo "<br>Uw belasting is: &euro;". $belasting;
}


$belasting = ($gewerkteuren < 40 ? "40%" : "45%");
echo "<br>Uw belasting is: ". $belasting;