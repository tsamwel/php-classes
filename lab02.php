<?php

$leerlingen = [
    [
    'naam' => "Karim", 
    'nederlands' => "8.5",
    'engels' => "7.7",
    'rekenen' => "6.7",
    'programmeren' => "8.5",
    'databases' => "9.4",
    'gemiddeld' => average((double)$nederlands, (double)$engels, (double)$rekenen, (double)$programmeren, (double)$databases);,
    ],
    [
    'naam' => "Sophie", 
    'nederlands' => "8.9",
    'engels' => "8.7",
    'rekenen' => "9.7",
    'programmeren' => "9.5",
    'databases' => "9.2",
    'gemiddeld' => average((double)$nederlands, (double)$engels, (double)$rekenen, (double)$programmeren, (double)$databases);,
    ],
];

$groepGemiddeld = array_sum([$leerlingen[0] gemiddeld, $leerlingen[1] gemiddeld]) / 2;

echo '<table border="1">
    <caption>
        <strong>Rapport</strong>
    </caption>
    <thead>
        <tr>
            <th>Naam</th>
            <th>Nederlands</th>
            <th>Engels</th>
            <th>Rekenen</th>
            <th>Programmeren</th>
            <th>Databases</th>
            <th>Gemiddeld</th>
        </tr>
    </thead>
    <body>
        <tr>
            <td>'.$naam.'</td>
            <td>'.$nederlands.'</td>
            <td>'.$engels.'</td>
            <td>'.$rekenen.'</td>
            <td>'.$programmeren.'</td>
            <td>'.$databases.'</td>
            <td>'.$gemiddeld.'</td>
        </tr>
        <tr>
            <td>'.$naam2.'</td>
            <td>'.$nederlands2.'</td>
            <td>'.$engels2.'</td>
            <td>'.$rekenen2.'</td>
            <td>'.$programmeren2.'</td>
            <td>'.$databases2.'</td>
            <td>'.$gemiddeld2.'</td>
        </tr>

    </body>
    <tfoot>
        <tr>
            <td colspan="6">Groep gemiddeld</td>
            <td>'.$groepGemiddeld.'</td>
        </tr>
    </tfoot>
</table>';


function average($nederlands, $engels, $rekenen, $programmeren, $databases)
{
    return array_sum([$nederlands, $engels, $rekenen, $programmeren, $databases]) / 5;
    return ($nederlands + $engels + $rekenen + $programmeren + $databases) / 5;
}