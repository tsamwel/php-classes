<?php

$boeken = array(
	array(
		"titel" => "stoner",
		"auteur" => "John Williams",
		"genre" => "fictie",
		"prijs" => "19.99"
	),
	array(
		"titel" => "de cirkel",
		"auteur" => "dave eggers",
		"genre" => "fictie",
		"prijs" => "22.50"
	),
	array(
		"titel" => "rayuela",
		"auteur" => "julio cortazar",
		"genre" => "fictie",
		"prijs" => "25.50"
	)
);

array_walk_recursive($boeken, function($value, $key) {
	if ($key == "prijs") {
		echo $key . ": " . $value . "<br>";
	}
});
// array_walk_recursive($boeken, function($value, $key) {
//     if ($key == 'prijs') {
//         echo 'prijs'.$value.'<br>';
//     }
// });