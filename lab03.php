<?php

$playlist = array(
	array(
		"genre" => "hiphop",
		"auteur" => "john williams",
		"titel" => "my girl",
	),
	array(
		"genre" => "jazz",
		"auteur" => "john coltrane",
		"titel" => "new york",
	),
	array(
		"genre" => "hiphop",
		"auteur" => "shakira",
		"titel" => "obsession",
	)
);

echo "Lab 03 <br> ---Stap 1: Mijn playlist:<br>";
function printArray($item, $key){
	echo "<br> $key" . ": " . "<i> $item </i>";
};
array_walk_recursive($playlist, 'printArray');

array_push($playlist, 
	array(
		"genre" => "latin",
		"auteur" => "caetano veloso",
		"titel" => "cafe atlantico",
	)
);
echo "<br> ---Stap 2: adding another song to the playlist: <br>";
array_walk_recursive($playlist, 'printArray');


echo "<br> <br> ---Stap 3: Tracks doorlopen: <br>";
function printTracks ($item, $key){
	echo "<br>" . "Track " . "$key" . ": " . implode(" | ", $item) ;
};
array_walk($playlist, 'printTracks');